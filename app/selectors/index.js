import global from 'global-object'

const exports = {
  topup: {},
  card: {},
  member: {}
}

// Card
exports.card = (s) => s.card

// Topup
exports.topup = (s) => s.topup
exports.topupRequested = (s) => s.topup.requested

// Member
exports.member.authenticated = () =>
  global.qff_auth.isInitialised() && global.qff_auth.isAuthenticated()
exports.member.authToken = () => global.qff_auth.getAccessToken()
exports.member.memberId = () => global.qff_auth.getMemberID()

export default exports
