/*  global ENV  */
import axios from 'axios'

const GET_POINTS = `${ENV.API}/product/topuppoints/` // GET current user's points data
const PURCHASE_POINTS = '/product/topuppoints/orders' // POST, PUT current pruchase and status

const TIME_OUT = 10000

/**
 * [fetchTopupPoints description]
 * @param  {String} memberId  ID of authorized member
 * @param  {String} authToken Generated Auth Token
 * @return {Object}           respnose object with error and fetched data
 */
const fetchTopupPoints = (memberId, authToken) => {
  return axios({
    url: GET_POINTS + `${memberId}`,
    timeout: TIME_OUT,
    method: 'GET',
    responseType: 'json',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  })
  .then(response => {
    return { error: null, response }
  })
  .catch(response => {
    return { error: response.error || 'error fetching topup points', response: null }
  })
}

/**
 * Post your points Purcahse
 * @param  {String} memberId  ID of authorized member
 * @param  {String} authToken Generated Auth Token
 * @param  {Object} data      Contains [POST] payload
 * @return {[type]}           [description]
 */
const postTopup = (memberId, authToken, data) => {
  return axios({
    url: PURCHASE_POINTS,
    timeout: TIME_OUT,
    method: 'POST',
    responseType: 'json',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    data: {
      creatorId: memberId,
      paymentSessionId: data.sessionId,
      points: data.totalPoints,
      pricing: {
        currency: 'au',
        price: data.totalCost
      }
    }
  })
  .then(response => {
    return { error: null, response }
  })
  .catch(response => {
    return { error: response.error || 'error posting topup points', response: null }
  })
}

export default {
  fetchTopupPoints,
  postTopup
}
