import { h } from 'preact'
import { connect } from 'preact-redux'
import store from '../store'

export default function connectWithStore (WrappedComponent, ...args) {
  let ConnectedWrappedComponent = connect(...args)(WrappedComponent)
  const wrapper = (props) => (
    <ConnectedWrappedComponent {...props} store={store} />
  )
  wrapper.DisplayName = `Connected${WrappedComponent.constructor.name}`
  return wrapper
}
