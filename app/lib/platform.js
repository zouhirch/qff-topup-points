import bowser from 'bowser'

export function isIE () {
  return bowser.msie || bowser.msedge
}

export function isSafari () {
  return bowser.safari
}
