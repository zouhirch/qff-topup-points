/* global ENV */

import { isIE } from '../lib/platform'

const prefix = 'qff-topup-points:'

const debugLogging = ((ENV.NODE_ENV !== 'prd') && (ENV.NODE_ENV !== 'stg'))
const verboseLogging = (ENV.NODE_ENV !== 'prd')

const Logger = {}
if (isIE()) {
  const log = Function.prototype.bind.call(console.log, console)

  if (debugLogging) {
    Logger.debug = (...args) => log(prefix + 'DEBUG: ', args)
  } else {
    Logger.debug = () => {}
  }
  if (verboseLogging) {
    Logger.log = (...args) => log(prefix + 'LOG: ', args)
  } else {
    Logger.log = () => {}
  }
  Logger.info = (...args) => log(prefix + 'INFO: ', args)
  Logger.warn = (...args) => log(prefix + 'WARN: ', args)
  Logger.error = (...args) => log(prefix + 'ERROR: ', args)
} else {
  if (debugLogging) {
    Logger.debug = (...args) => console.debug(prefix, ...args)
  } else {
    Logger.debug = () => {}
  }
  if (verboseLogging) {
    Logger.log = (...args) => console.log(prefix, ...args)
  } else {
    Logger.log = () => {}
  }
  Logger.info = (...args) => console.info(prefix, ...args)
  Logger.warn = (...args) => console.warn(prefix, ...args)
  Logger.error = (...args) => console.error(prefix, ...args)
}

export default Logger
