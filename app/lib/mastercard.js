// TODO: update with new logger module
const formSessionUpdated = (response, next) => {
  if (!response.status) {
    return { type: 'session_error', error: 'Session update failed' }
  }
  switch (response.status) {
    case 'ok':
      return next({ type: response.status, sessionId: response.session.id })
    case 'fields_in_error':
      return next({ type: response.status, error: response.errors })
    case 'request_timeout':
      return next({ type: response.status, error: response.errors })
    case 'system_error':
      return next({ type: response.status, error: response.errors })
    default:
      return next({ type: 'session_error', error: 'Session update failed' })
  }
}

const cardInit = (response) => {
  return response.status === 'ok' ? { sessionInit: true } : { sessionInit: false }
}

export { cardInit, formSessionUpdated }
