import { h, Component } from 'preact'
import PropTypes from 'proptypes'
import selectors from '../selectors'

export default class Widget extends Component {

  static childContextTypes = {
    widgetRef: PropTypes.string.isRequired
  }

  static defaultProps = {
    loadError: null
  }

  static mapStateToProps = (state) => ({
    authenticated: selectors.member.authenticated(state)
  })

  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    loadError: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    widgetRef: PropTypes.string
  }

  state = {
    widgetRef: this.props.widgetRef || Math.random().toString(36).substr(2, 9)
  }

  getChildContext () {
    return {
      widgetRef: this.state.widgetRef
    }
  }

  render () {
    if (!this.props.authenticated) {
      return <span>awaiting authentication</span>
    }
    if (this.props.loadError) {
      return <span>error: {this.props.loadError}</span>
    }
    return this.renderWidget()
  }
}
