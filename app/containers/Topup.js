// theirs
import { h } from 'preact'

// ours
// components
import Widget from './Widget'
import SliderPanel from '../components/SliderPanel'
import Creditcard from '../components/Creditcard'
import Total from '../components/Total'
import Loading from '../components/Loading'

// redux
import connectWithStore from '../lib/connectWithStore'
import actions from '../actions'
import selectors from '../selectors'
import { formSessionUpdated, cardInit } from '../lib/mastercard'

/* global  PaymentSession */
class Topup extends Widget {
  static mapStateToProps = (state) => ({
    ...Widget.mapStateToProps(state),
    ...selectors.topup(state),
    ...selectors.card(state),
    memberId: selectors.member.memberId(state)
  })
  componentDidMount () {
    this.props.dispatch(actions.topup.loadRequest())
  }

  updateTopupTotals = (newState) => {
    this.props.dispatch(actions.topup.update(newState))
  }

  updateCreditCardForm = () => {
    PaymentSession.updateSessionFromForm('card')
  }

  componentDidUpdate () {
    const CARD = {
      number: '#card-number',
      securityCode: '#security-code',
      expiryMonth: '#expiry-month',
      expiryYear: '#expiry-year'
    }
    if (this.props.sessionInit || !this.base.querySelector(CARD.number)) {
      return
    }
    this.props.dispatch(actions.card.update({ sessionInit: true }))
    const FRAME_EMBEDDING = 'javascript'
    const ORDER = {
      amount: this.props.totalCost || 0,
      currency: 'AUD'
    }
    PaymentSession.configure({
      fields: {
        card: CARD
      },
      frameEmbeddingMitigation: [FRAME_EMBEDDING],
      callbacks: {
        initialized: (response) => this.props.dispatch(actions.card.update(cardInit(response))),
        formSessionUpdate: (response) => { // normal function because need `this` to work
          formSessionUpdated(response, (res) => {
            if (res.type === 'ok' || res.type === 'fields_in_error') {
              this.props.dispatch(actions.card.update(res))
            }
            res.error.nonField = res.type
            return this.props.dispatch(actions.card.update(res))
          })
        }
      },
      order: ORDER
    })
  }
  renderWidget () {
    if (this.props.loading) {
      return <Loading label='Loading your points' />
    }
    return (
      <div>
        <SliderPanel {...this.props.pointsData} pricingOptions={this.props.pricingOptions} updateTopupTotals={this.updateTopupTotals} />
        <Creditcard {...this.props} />
        <Total {...this.props} updateCreditCardForm={this.updateCreditCardForm} />
      </div>
    )
  }
}

export default connectWithStore(Topup, Topup.mapStateToProps)
