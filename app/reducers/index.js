import TopupReducer from './TopupReducer'
import CardReducer from './CardReducer'
import combineReducers from 'redux/lib/combineReducers'

export default combineReducers({
  card: CardReducer,
  topup: TopupReducer
})
