import Immutable from 'seamless-immutable'
import constants from '../constants'
import handleActions from 'redux-actions/lib/handleActions'

const initialState = new Immutable({
  sessionId: false,
  sessionInit: false,
  errorCardNumber: null,
  errorExpiryYear: null,
  errorExpiryMonth: null,
  errorSecurityCode: null,
  errorNonField: null
})

const handleReset = (state, action) => initialState.merge({
  requested: state.requested // preserve requested across login/logout
})

export default handleActions({

  // Reset on member authentication changes in case the logged in user and our
  // data doesn't match.
  [constants.action.member.authenticated]: handleReset,
  [constants.action.member.logoff]: handleReset,

  [constants.action.card.update]: (state, action) => (
    state.merge(action.payload)
  )
}, initialState)
