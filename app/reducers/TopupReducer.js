import Immutable from 'seamless-immutable'
import constants from '../constants'
import handleActions from 'redux-actions/lib/handleActions'

const initialState = new Immutable({
  loadError: null,
  loading: false,
  requested: false,
  pointsData: {},
  topupPricing: [],
  totalCost: null,
  totalPoints: null
})

const handleReset = (state, action) => initialState.merge({
  requested: state.requested // preserve requested across login/logout
})

export default handleActions({

  // Reset on member authentication changes in case the logged in user and our
  // data doesn't match.
  [constants.action.member.authenticated]: handleReset,
  [constants.action.member.logoff]: handleReset,

  [constants.action.topup.load.request]: (state) => (
    state.merge({
      requested: true
    })
  ),

  [constants.action.topup.load.pending]: (state) => (
    state.merge({
      loadError: null,
      loading: true
    })
  ),

  [constants.action.topup.load.failure]: (state, action) => (
    state.merge({
      loadError: action.error,
      loading: false
    })
  ),

  [constants.action.topup.load.success]: (state, action) => (
    state.merge(action.payload).merge({
      loadError: null,
      loading: false
    })
  ),

  [constants.action.topup.update]: (state, action) => (
    state.merge({
      totalCost: action.payload.totalCost,
      totalPoints: action.payload.totalPoints
    })
  )
}, initialState)
