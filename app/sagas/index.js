import * as TopupSaga from './TopupSaga'
import * as MemberSagas from './MemberSagas'

import Logger from '../lib/Logger'
import constants from '../constants'
import { takeEvery } from 'redux-saga'

export default function * root () {
  yield [
    takeEvery(constants.action.topup.load.request, TopupSaga.load),
    takeEvery(constants.action.member.authenticated, MemberSagas.authenticated)
  ]

  Logger.info('saga: initialisation completed')
}
