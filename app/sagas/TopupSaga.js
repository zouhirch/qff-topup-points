// theirs
import { call, put, select } from 'redux-saga/effects'

// ours
import Logger from '../lib/Logger'
import actions from '../actions'
import selectors from '../selectors'
import api from '../services/api'

export function *load () {
  Logger.log('PointsSaga:load()')
  yield put(actions.topup.loadPending())

  // Check if we are authenticated
  const authenticated = yield select(selectors.member.authenticated)
  const memberId = yield select(selectors.member.memberId)
  const authToken = yield select(selectors.member.authToken)

  if (!authenticated || !memberId || !authToken) {
    yield put(actions.topup.loadFailure('AUTHENTICATION_REQUIRED'))
    Logger.log('PointsSaga:load() failed: AUTHENTICATION_REQUIRED')
    return
  }

  const {response, error} = yield call(api.fetchTopupPoints, memberId, authToken)

  if (error) {
    yield put(actions.topup.loadFailure('RESPONSE_ERROR'))
    Logger.log('PointsSaga:load() failed: RESPONSE_ERROR')
    return
  }

  let { pricingOptions, ...pointsData } = response.data

  yield put(actions.topup.loadSuccess({
    'pointsData': pointsData,
    'pricingOptions': pricingOptions
  }))

  Logger.log('PointsSaga:load() completed')
}

/**
 * Update Saga
 */
