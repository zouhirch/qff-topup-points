import { put, select } from 'redux-saga/effects'

import Logger from '../lib/Logger'
import actions from '../actions'
import selectors from '../selectors'

export function * authenticated () {
  Logger.log('MemberSagas:authenticated()')

  // Trigger loading of categories data if previously requested
  const topupRequested = yield select(selectors.topupRequested)
  Logger.debug('MemberSagas:authenticated() topupPointsRequested', topupRequested)
  if (topupRequested) {
    yield put(actions.topup.loadRequest())
  }

  Logger.log('MemberSagas:authenticated() completed')
}
