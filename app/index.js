/* global ENV  */
import Logger from './lib/Logger'
import actions from './actions'
import global from 'global-object'
import store from './store'
import habitat from '@qantasloyalty/habitat'

// our widgets
import Topup from './containers/Topup'

Logger.info(`app: version ${ENV.VERSION}`)

Logger.debug('app: adding qff_auth callbacks')

// Define callbacks for auth state change
function qffPointsActivityOnMemberAuthenticated () { store.dispatch(actions.member.authenticated()) }
function qffPointsActivityOnMemberLogoff () { store.dispatch(actions.member.logoff()) }

// Subscribe to QFF Auth API callbacks to facilate auth integration
global.qff_auth.subscribeInitAuthenticated(qffPointsActivityOnMemberAuthenticated)
global.qff_auth.subscribeLoginSuccess(qffPointsActivityOnMemberAuthenticated)
global.qff_auth.subscribeLogoutSuccess(qffPointsActivityOnMemberLogoff)

// If the user is already authenticated then trigger it manually as there will
// be no initial callback.
if (global.qff_auth.isInitialised() && global.qff_auth.isAuthenticated()) {
  qffPointsActivityOnMemberAuthenticated()
}

Logger.debug('app: bootstrapping container')

function init () {
  let habitatTopup = habitat(Topup)
  document.addEventListener('habitat-ready', () => {
    habitatTopup.render({attrValue: 'sample-widget', attrKey: 'data-qff-hello'})
  }, false)
}

init()

Logger.info('app: initialisation completed')
