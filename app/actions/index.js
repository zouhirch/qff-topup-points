import constants from '../constants'

export default {
  // TODO: sample - change or remove if not needed
  topup: {
    loadRequest: actionRequest(constants.action.topup.load),
    loadPending: actionPending(constants.action.topup.load),
    loadFailure: actionFailure(constants.action.topup.load),
    loadSuccess: actionSuccess(constants.action.topup.load),
    update: (payload) => action(constants.action.topup.update, payload)
  },
  card: {
    failure: (error) => action(constants.action.card.failure, null, error),
    update: (payload) => {
      const newState = {}
      if (payload.sessionId) {
        newState.sessionId = payload.sessionId || null
      }
      if (payload.sessionInit) {
        newState.sessionInit = payload.sessionInit
      }
      return action(constants.action.card.update, {
        ...newState,
        errorCardNumber: (payload.error || {}).cardNumber || null,
        errorExpiryYear: (payload.error || {}).cardNumber || null,
        errorExpiryMonth: (payload.error || {}).expiryMonth || null,
        errorSecurityCode: (payload.error || {}).securityCode || null,
        errorNonField: (payload.error || {}).nonField || null
      })
    }
  },
  member: {
    authenticated: () => action(constants.action.member.authenticated),
    logoff: () => action(constants.action.member.logoff)
  }
}

function action (type, payload, error) {
  if (!type) {
    throw new Error("missing mandatory parameter: 'type'")
  }

  const data = {
    type,
    meta: {
      timestamp: Date.now()
    }
  }

  if (payload) { data.payload = payload }
  if (error) { data.error = error }

  return data
}

function actionRequest (type) {
  return (payload) => action(type.request, payload)
}

function actionPending (type) {
  return (payload) => action(type.pending, payload)
}

function actionFailure (type) {
  return (error, payload) => action(type.failure, payload, error)
}

function actionSuccess (type) {
  return (payload, error) => action(type.success, payload, error)
}
