// theirs
import { h, Component } from 'preact'

// ours
import styles from './style.scss'

let DEFAULT_LABEL = 'Loading...'

export default class Loading extends Component {
  render (props) {
    return (
      <div class={styles.loaderWrapper}>
        <div class={styles.loader} aria-valuetext={props.label || DEFAULT_LABEL} aria-role='progressbar'>
          <span class={styles.label}>{props.label || DEFAULT_LABEL}</span>
          <div class={`${styles.skThreeBounce} ${styles.loaderIcon}`}>
            <div class={styles.skBounce1} />
            <div class={styles.skBounce2} />
            <div class={styles.skBounce3} />
          </div>
        </div>
      </div>
    )
  }
}
