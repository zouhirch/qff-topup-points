import { h, Component } from 'preact'

export default class Hello extends Component {
  static defaultProps = {
    pointsBalance: 'N/A',
    totalPoints: 'N/A'
  }
  render (props) {
    let { totalPoints, style } = props
    let { pointsBalance } = props
    return (
      <div>
        <h1>Purchase top up points</h1>
        <div class={style.panel}>
          <div class={`${style.row} ${style.center}`}>
            <div class={style.half}>
              Your current points balance: <span>{pointsBalance}</span>
            </div>
            <div class={style.half}>
              Point balance after purchase: <span>{totalPoints}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
