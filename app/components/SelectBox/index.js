// theirs
import { h, Component } from 'preact'

export default class SelectBox extends Component {
  static defaultProps = {
    type: 'year'
  }
  populateMonths = () => {
    let months = []
    for (let i = 1; i <= 12; i++) {
      months.push(`0${i}`.slice(-2))
    }
    return months
  }
  populateYears = () => {
    let years = []
    let currentYear = +new Date().getFullYear().toString().substr(2, 2)
    for (var i = 0; i < 10; i++) {
      years.push(currentYear + i)
    }
    return years
  }
  optsPlaceHolder = (type) => {
    return type === 'month' ? 'MM' : 'YY'
  }
  render (props) {
    let { type } = props
    type = type.toLowerCase()
    const OPTIONS = type === 'year' ? this.populateYears() : this.populateMonths()
    return (
      <select id={`expiry-${type}`}>
        <option disabled selected>{this.optsPlaceHolder(type)}</option>
        {
          OPTIONS.map(opt => (
            <option value={opt}>{opt}</option>
          ))
        }
      </select>
    )
  }
}
