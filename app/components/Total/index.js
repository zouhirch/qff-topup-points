import { h, Component } from 'preact'

import style from './style'

export default class Total extends Component {
  state = {
    checked: false
  }
  handleSubmit = () => {
    this.props.updateCreditCardForm()
  }
  render (props) {
    let { totalCost } = props
    return (
      <section class={style.topUpWidget}>
        <div class={style.panel + ' ' + style.wider}>
          <div class={style.row}>
            {/* label half */}
            <div class={style.half}>
              <p>
                <strong>Total amount payable including GST:</strong>
              </p>
            </div>
            {/* value half */}
            <div class={style.half}>
              <h3>${totalCost}</h3>
            </div>
          </div>
          <div class={style.row}>
            <div class={style.half}>
              <button onClick={this.handleSubmit}>Pay Now</button>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
