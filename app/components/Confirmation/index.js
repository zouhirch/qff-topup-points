// theirs
import { h, Component } from 'preact'

// ours
import style from './style.scss'

export default class Confirmation extends Component {
  render (props) {
    return (
      <section class={style.topUpWidget}>
        <h1>Confirmation</h1>
        <div class={style.confirmTable}>
          <table>
            <tr>
              <td class={style.title}>
                KEY
              </td>
              <td class={style.value}>
                VALUE
              </td>
            </tr>
          </table>
        </div>
        <button class={style.confirmBtn}>
          Print Confirmation
        </button>
        <p class={style.confirmNote}>Note: an invoice will be emailed to your preffered email address</p>
      </section>
    )
  }
}
