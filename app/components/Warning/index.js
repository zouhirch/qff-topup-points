import { h, Component } from 'preact'

import style from './style'

export default class Warning extends Component {
  render (props) {
    let { eligible, pointsBalance, message } = props
    if (typeof eligible !== 'undefined' && !eligible) {
      return (
        <div class={style.fixedRow}>
          <div class={style.warning}>
            <p class={style.title}>You{`'`}re unable to purchase points at this time</p>
            <p class={style.message}>You have topped up twice this year, the maximum allowed within a 12 months period</p>
          </div>
        </div>
      )
    }
    if (typeof pointsBalance !== 'undefined' && pointsBalance <= 2000) {
      return (
        <div class={style.fixedRow}>
          <div class={style.warning + ' ' + style.infoGreen}>
            <p class={style.message}>As your points balance is 2,000 or less, the maximum you can purchase is 500</p>
          </div>
        </div>
      )
    }
    if (typeof message !== 'undefined' && message) {
      return (
        <div class={style.fixedRow}>
          <div class={style.warning}>
            <p class={style.message}>{message}</p>
          </div>
        </div>
      )
    }
    return null
  }
}
