import { h, Component } from 'preact'

import Slider from 'react-toolbox/lib/slider'
import theme from './sliderTheme'

export default class SliderPanel extends Component {
  sliderChanged = (value) => {
    console.log(value)
    this.props.handleSliderChange(value)
  }
  render (props) {
    let { style, minPoints, stepPoints, maxPoints, pruchasedPoints, pointsBalance } = props
    return (
      <div class={style.fixedRow + ' ' + style.sliderRow}>
        <Slider theme={theme}
          step={stepPoints}
          min={minPoints}
          max={maxPoints}
          value={pruchasedPoints}
          onChange={this.sliderChanged}
          disabled={pointsBalance <= 2000} />
      </div>
    )
  }
}
