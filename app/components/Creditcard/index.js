import { h, Component } from 'preact'
import style from './style'

import SelectBox from '../SelectBox'
import TextBox from '../TextBox'

import actions from '../../actions'

export default class CreditCard extends Component {
  componentWillUnmount () {
    this.props.dispatch(actions.card.update({ sessionInit: false }))
  }
  render (props) {
    let { errorNonField, errorCardNumber, errorSecurityCode, errorExpiryMonth, errorExpiryYear } = props
    return (
      <section class={style.topUpWidget}>
        <h1>Credit card details</h1>
        <div class={style.panel}>
          <p>{errorNonField ? `Error: ${errorNonField}` : ` `}</p>
          <div class={style.row}>
            <div class={style.half}>
              <TextBox id='card-name' label='Card holder name' />
            </div>
            <div class={style.half}>
              <TextBox id='card-number' label='Card number' error={errorCardNumber ? `invalid card number` : null} readOnly />
            </div>
          </div>
          <div class={style.row}>
            <div class={style.half}>
              {/* 1 label for 2 inputs as per design leave it here please */}
              <label>Expiry Date (MM/YY)</label>
              <div class={style.row}>
                <div class={style.half}>
                  <SelectBox type='month' />
                </div>
                <div class={style.half + ' ' + style.disableMb}>
                  <SelectBox type='year' />
                </div>
              </div>
              <span class={style.error}>{ errorExpiryMonth || errorExpiryYear ? `invalid expiry date` : null }</span>
            </div>
            <div class={style.half}>
              <div class={style.row}>
                <div class={style.half}>
                  <TextBox id='security-code' label='Security Code' error={errorSecurityCode ? `invalid security code` : null} readOnly />
                </div>
              </div>
            </div>
          </div>
        </div>{/* panel ends */}
      </section>
    )
  }
}
