// theirs
import { h, Component } from 'preact'
import style from './style'

export default class TextBox extends Component {
  render (props) {
    let { label, error, readOnly, id } = props
    return (
      <div>
        <label>{label}</label>
        <div>
          <input id={id} type='text' readOnly={readOnly} />
        </div>
        <span class={style.error}>{ error }</span>
      </div>
    )
  }
}
