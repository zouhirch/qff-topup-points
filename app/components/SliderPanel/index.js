import { h, Component } from 'preact'

import style from './style'

import Warning from '../Warning'
import SliderTitle from '../SliderTitle'
import Slider from '../Slider'

export default class SliderPanel extends Component {
  static defaultProps = {
    minPoints: 500,
    maxPoints: null,
    stepPoints: 1,
    eligible: true,
    pointsBalance: null,
    pricingOptions: []
  }
  state = {
    pruchasedPoints: null,
    pricingOptionsTransformed: [],
    totalCost: 'N/A',
    totalPoints: 'N/A'
  }
  componentDidMount = () => {
    const pricingOptionsTransformed = this.transformPricingOptions(this.props.pricingOptions)
    const newState = {
      pricingOptionsTransformed: pricingOptionsTransformed,
      totalCost: pricingOptionsTransformed[String(this.props.minPoints)],
      pruchasedPoints: this.props.minPoints,
      totalPoints: this.props.pointsBalance + this.props.minPoints
    }
    this.setState(newState)
    this.props.updateTopupTotals(this.state)
  }
  transformPricingOptions = (options) => {
    let ptsObj = {}
    this.props.pricingOptions.forEach((opt) => {
      ptsObj[String(opt.points)] = opt.gross
    })
    return ptsObj
  }
  handleSliderChange = (value) => {
    const newState = {}

    newState['pruchasedPoints'] = value
    newState['totalCost'] = this.state.pricingOptionsTransformed[value]
    newState['totalPoints'] = this.props.pointsBalance + value

    this.setState(newState)
    this.props.updateTopupTotals(this.state)
  }
  validatePointsValue = (value) => {
    const roundedUpVal = Math.ceil(value / this.props.stepPoints) * this.props.stepPoints
    return roundedUpVal > this.props.maxPoints ? this.props.maxPoints : roundedUpVal
  }
  handleInputChange = (event) => {
    const newState = {}
    newState['pruchasedPoints'] = this.validatePointsValue(+event.target.value)
    this.setState(newState)
  }
  render (props) {
    let { minPoints, maxPoints, eligible, pointsBalance } = props
    return (
      <div class={style.topUpWidget}>
        <SliderTitle style={style} {...props} totalPoints={this.state.totalPoints} />
        <div class={style.panel + ' ' + style.darker}>
          <h1>Number of points you wish to purchase</h1>
          <div class={style.fixedRow}>
            <div class={style.sliderLabel}>Min: <span>{minPoints}</span></div>
            <div class={style.sliderLabel}>Max: <span>{maxPoints}</span></div>
          </div>
          {/* Slider Component */}
          <Slider {...props} handleSliderChange={this.handleSliderChange} style={style} pruchasedPoints={this.state.pruchasedPoints} />
          {/* warning box */}
          <Warning pointsBalance={pointsBalance} eligible={eligible} />
          {/* end: warning box */}
          <div class={style.fixedRow}>
            <div class={style.row}>
              <div class={style.half}>
                <label>Number of points </label>
                <input type='number'
                  min={this.state.sliderMin}
                  max={this.state.sliderMax}
                  value={this.state.pruchasedPoints}
                  onBlur={this.handleInputChange} />
              </div>
              <div class={style.half}>
                <label>Total Cost </label>
                <input type='text'
                  value={`$${this.state.totalCost}`}
                  readOnly />
              </div>
            </div>
          </div>
          <div class={style.footNote}>
            No points must be purchased in etc askjdhasd ajshd.. etc.., see <a href='#'>Pricing details</a> for more etc etc
          </div>
        </div>
      </div>
    )
  }
}
