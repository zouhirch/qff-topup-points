import ActionConstants from './ActionConstants'
import ConfigConstants from './ConfigConstants'

export default {
  action: ActionConstants,
  config: ConfigConstants
}
