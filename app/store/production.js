import applyMiddleware from 'redux/lib/applyMiddleware'
import compose from 'redux/lib/compose'
import createSagaMiddleware from 'redux-saga'
import createStore from 'redux/lib/createStore'
import rootReducer from '../reducers'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const createStoreWithMiddleware = compose(
  applyMiddleware(
    sagaMiddleware,
  ),
)(createStore)

const store = createStoreWithMiddleware(rootReducer)

sagaMiddleware.run(rootSaga)

export default store
