/*  global ENV_DEV ENV_TST  */

if (ENV_DEV || ENV_TST) {
  module.exports = require('./development')
} else {
  module.exports = require('./production')
}
