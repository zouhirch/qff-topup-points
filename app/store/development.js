import applyMiddleware from 'redux/lib/applyMiddleware'
// import compose from 'redux/lib/compose'
import { composeWithDevTools } from 'redux-devtools-extension'
import createLogger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import createStore from 'redux/lib/createStore'
import rootReducer from '../reducers'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const createStoreWithMiddleware = composeWithDevTools(
  applyMiddleware(
    createLogger(),
    sagaMiddleware,
  ),
)(createStore)

const store = createStoreWithMiddleware(rootReducer)

sagaMiddleware.run(rootSaga)

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('../reducers', () => {
    const nextRootReducer = require('../reducers')

    store.replaceReducer(nextRootReducer)
  })
}

export default store
