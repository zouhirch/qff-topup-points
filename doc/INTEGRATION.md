QFF Points Activity App: Integration Guide
==========================================

Integration Steps
-----------------

A full page example is available at: `https://apps-stg.qantasloyalty.com/qff-points-activity/examples/index.html`

1.	Load application bundle:

	```
	<head>
	    <!-- ensure login widget's init.bundle.js is loaded here -->
	</head>
	<body>
	    <!-- ensure login widget's login.bundle.js is loaded here -->
	    <script async src="https://apps.qantasloyalty.com/qff-points-activity/index.bundle.js"></script>
	    <!-- body content -->
	</body>
	```

2.	Add widget placeholders:

	You may add multiple widgets by repeating this step.

	```
	<div data-qff-points-activity="activity-statement">
	    Loading ... (placeholder content while widget is loading)
	</div>
	```

### Dependencies

#### QFF Login Widget

The QFF Points Activity App is dependent on the QFF Login Widget as it integrates with the `window.qff_auth` API interface for authentication.

Please ensure:

-	the QFF Login Widget's `init.bundle.js` is loaded in the head section of the page
-	the QFF Login Widget's `login.bundle.js` is loaded somewhere in the page

The QFF Login Widget environment (production vs non-production) must match the QFF Points Activity App environment for authentication to function correctly.

### Resources

#### Production

The production version of the QFF Points Activity App integrates with other production services and requires valid QFF member credentials. The production version of the QFF Login Widget must be used in this environment.

Resources:

-	`https://apps.qantasloyalty.com/qff-points-activity/index.bundle.js`

#### Non Production

The non-production version of the QFF Points Activity App integrates with other non-production services and requires test QFF member credentials. The non production version of the QFF Login Widget must be used in this environment.

Resources:

-	`https://apps.qantasloyalty-staging.com/qff-points-activity/index.bundle.js`

Javascript API
--------------

The QFF Points Activity app does not expose any API integrations.

Widgets
-------

The QFF Points Activity App exposes the following widgets which can be loaded by specifying the Widget ID as the value of the `data-qff-points-activity` attribute on a `span` or `div` node.

| Widget ID            | Widget Name        |
|:--------------------:|--------------------|
| `activity-statement` | Activity Statement |
| `points-by-category` | Points by Category |

### Activity Statement Widget

The Points by Category widget ... TODO

#### Configuration

The Activity Statement widget does not offer any configuration parameters.

### Points by Category Widget

The Points by Category widget ... TODO

#### Configuration

The Points by Category widget does not offer any configuration parameters.
