const ExtractTextPlugin = require('extract-text-webpack-plugin')
const version = require('@qantasloyalty/version-manager')
const webpack = require('webpack')
const ReplacePlugin =  require('replace-bundle-webpack-plugin')
const path =  require('path')

const apiENV = (process.env.NODE_ENV === 'dev'  || process.env.NODE_ENV === 'tst') ? 'sit' : process.env.NODE_ENV

const ENV = {
  API: process.env.API_URI
    ? process.env.API_URI
    : `https://api.services-${apiENV}.qantasloyalty.com`,
  NODE_ENV: process.env.NODE_ENV,
  VERSION: version.full()
}

const CSS_MAPS = process.env.NODE_ENV !== 'prd'

module.exports = {
  context: path.resolve(__dirname, 'app'),
  entry: './index.js',

  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle.js',
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: ['', '.jsx', '.js', '.json', '.css', '.scss'],
    modulesDirectories: [
      path.resolve(__dirname, 'app/lib'),
      path.resolve(__dirname, 'node_modules'),
      'node_modules'
    ],
    alias: {
      src: path.resolve(__dirname, 'app'), // used for tests
      styles: path.resolve(__dirname, 'app/styles'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat'
    }
  },
  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        exclude: path.resolve(__dirname, 'app'),
        loader: 'source-map'
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        // Transform our own .(scss|css) files with PostCSS and CSS-modules
        test: /\.(scss|css)$/,
        include: [path.resolve(__dirname, 'app')],
        loader: [
          `style-loader?singleton`,
          `css-loader?modules&importLoaders=1&localIdentName=[local]${process.env.CSS_MODULES_IDENT || '_[hash:base64:5]'}&sourceMap=${CSS_MAPS}`,
          'postcss-loader',
          `sass-loader?sourceMap=${CSS_MAPS}`
        ].join('!')
      },
      {
        test: /\.(scss|css)$/,
        exclude: [path.resolve(__dirname, 'app')],
        loader: [
          `style-loader?singleton`,
          `css-loader?modules&importLoaders=1&localIdentName=[local]${process.env.CSS_MODULES_IDENT || '_[hash:base64:5]'}&sourceMap=${CSS_MAPS}`,
          `sass-loader?sourceMap=${CSS_MAPS}`,
          `postcss`
        ].join('!')
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.(xml|html|txt|md)$/,
        loader: 'raw'
      },
      {
        test: /\.(svg|woff2?|ttf|eot|jpe?g|png|gif)(\?.*)?$/i,
        loader: 'url'
      }
    ]
  },
  plugins: ([
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      ENV: JSON.stringify(ENV),
      ENV_DEV: process.env.NODE_ENV === 'dev',
      ENV_TST: process.env.NODE_ENV === 'tst',
      ENV_STG: process.env.NODE_ENV === 'stg',
      ENV_PRD: process.env.NODE_ENV === 'prd'
    })
  ]).concat(ENV === 'prd' ? [
   // strip out babel-helper invariant checks
    new ReplacePlugin([{
      // this is actually the property name https://github.com/kimhou/replace-bundle-webpack-plugin/issues/1
      partten: /throw\s+(new\s+)?[a-zA-Z]+Error\s*\(/g,
      replacement: () => 'return;('
    }])
  ] : []),

  stats: { colors: true },

  node: {
    global: true,
    process: true,
    Buffer: false,
    __filename: false,
    __dirname: false,
    setImmediate: false
  },

  // dev
  devtool: process.env.NODE_ENV === 'prd' ? 'source-map' : 'cheap-module-eval-source-map',
  devServer: {
    port: process.env.PORT || 8080,
    host: 'localhost',
    colors: true,
    publicPath: '/public',
    contentBase: './public',
    historyApiFallback: true,
    open: true
  }
}
