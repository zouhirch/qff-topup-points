QFF Point Activity Widget
=========================

The documents external API changes. It is not meant to be consumed directly and is instead used with `git blame` to match each line in the file back to a commit and tagged release.

To generate a more meaningful changelog run: `npm run changelog`

Changelog
---------

-	basic shell for Activity Statement widget
-	basic shell for Points by Category widget
-	integration with QFF Auth API for member authentication
-	added semantic versioning
