const CopyWebpackPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const version = require('@qantasloyalty/version-manager')
const webpack = require('webpack')

const ENV = {
  // please rename this
  API: process.env.NODE_ENV === 'prd'
    ? 'https://api.services.qantasloyalty.com'
    : `https://api.services-${process.env.NODE_ENV}.qantasloyalty.com`,
  NODE_ENV: process.env.NODE_ENV,
  VERSION: version.full()
}

module.exports = {

  entry: {
    index: [
      './app/index.js'
    ]
  },

  devtool: 'source-map',
  debug: true,

  output: {
    path: './public',
    pathInfo: true,
    publicPath: '/',
    filename: '[name].bundle.js',
    chunkFilename: '[id].bundle.js'
  },

  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        loader: 'source-map-loader',
        include: [/app/],
        exclude: [/bower_components/, /node_modules/]
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: [/bower_components/, /node_modules/]
      },
      {
        test: /\.jsx$/,
        loaders: ['babel-loader'],
        exclude: [/bower_components/, /node_modules/]
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css')
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract('style', 'css!less')
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css!sass')
      },

      // the url-loader uses DataUrls.
      // the file-loader emits files.
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+|\?[a-z0-9]+)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+|\?[a-z0-9]+)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+|\?[a-z0-9]+)?$/,
        loader: 'file'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+|\?[a-z0-9]+)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /.json$/,
        loader: 'json'
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.wav$|\.mp3$/,
        loader: 'file'
      }
    ]
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: 'public' }
    ]),
    new ExtractTextPlugin('[name].bundle.css'),
    new webpack.DefinePlugin({
      ENV: JSON.stringify(ENV),
      ENV_DEV: process.env.NODE_ENV === 'dev',
      ENV_TST: process.env.NODE_ENV === 'tst',
      ENV_STG: process.env.NODE_ENV === 'stg',
      ENV_PRD: process.env.NODE_ENV === 'prd',
      // Only used for react prod bundle. Refer to ENV.NODE_ENV for business logic
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
     // Eliminate comments
      comments: false,

     // Compression specific options
      compress: {
        warnings: false,

       // Drop `console` statements
        drop_console: true
      }
    }),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
      Promise: 'es6-promise-promise'
    })
  ]
}
